package com.example.demo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SomeFilter {

    public static List<SomeObject> filter(List<SomeObject> inputs) {
        List<SomeObject> result = new ArrayList<>();
        if (inputs.isEmpty()) {
            return result;
        }
        boolean isAllObjectsWithNullValues = inputs
                .stream()
                .allMatch(input -> input.getFirst() == null && input.getSecond() == null);
        if (isAllObjectsWithNullValues) {
            result.add(new SomeObject(null, null));
            return result;
        }

        Set<SomeObject> fullObjects = new HashSet<>();
        Set<SomeObject> leftObjects = new HashSet<>();
        Set<SomeObject> rightObjects = new HashSet<>();
        for (SomeObject input : inputs) {
            if (input.getFirst() != null && input.getSecond() != null) {
                fullObjects.add(input);
            } else if (input.getFirst() != null) {
                leftObjects.add(input);
            } else if (input.getSecond() != null) {
                rightObjects.add(input);
            }
        }

        result.addAll(fullObjects);

        for (SomeObject object : leftObjects) {
            boolean existSameFirstNumber = fullObjects
                    .stream()
                    .anyMatch(r -> r.getFirst().equals(object.getFirst()));
            if (!existSameFirstNumber) {
                result.add(object);
            }
        }
        for (SomeObject object : rightObjects) {
            boolean existSameSecondNumber = fullObjects
                    .stream()
                    .anyMatch(r -> r.getSecond().equals(object.getSecond()));
            if (!existSameSecondNumber) {
                result.add(object);
            }
        }
        return result;
    }
}
