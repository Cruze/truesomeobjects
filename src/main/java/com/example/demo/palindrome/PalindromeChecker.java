package com.example.demo.palindrome;

import java.math.BigDecimal;

public class PalindromeChecker {

    public static boolean isPalindrome(BigDecimal number) {
        String s = String.valueOf(number);
        return s.equals(new StringBuilder(s).reverse().toString());
    }
}
