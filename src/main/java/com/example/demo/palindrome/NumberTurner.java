package com.example.demo.palindrome;

import java.math.BigDecimal;

public class NumberTurner {

    public static BigDecimal turnNumber(BigDecimal number) {
        String s = String.valueOf(number);
        return new BigDecimal(new StringBuilder(s).reverse().toString());
    }

}
