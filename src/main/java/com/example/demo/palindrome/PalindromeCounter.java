package com.example.demo.palindrome;

import java.math.BigDecimal;

import static com.example.demo.palindrome.NumberTurner.*;
import static com.example.demo.palindrome.PalindromeChecker.*;

public class PalindromeCounter {

    public static int getStepCount(int number) {
        return getStepCount(BigDecimal.valueOf(number), 0);
    }

    private static int getStepCount(BigDecimal number, int step) {
        if (step > 20) {
            throw new MoreThan20StepsException();
        }
        if (isPalindrome(number)) {
            return step;
        } else {
            BigDecimal newNumber = number.add(turnNumber(number));
            return getStepCount(newNumber, step + 1);
        }
    }


}
