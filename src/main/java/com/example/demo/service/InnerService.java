package com.example.demo.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service
public class InnerService {
    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = DemoException.class)
    public void saveSomething() {
        throw new DemoException("demo exception");
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
