package com.example.demo.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MainService {
    private final InnerService innerService;

    public MainService(InnerService innerService) {
        this.innerService = innerService;
    }

    @Transactional(noRollbackFor = DemoException.class)
    public void saveSomething(SomeInterface someInterface) {
        someInterface.sout("hello!!!!!!!!!!");
        try {
            innerService.saveSomething();
        } catch (DemoException e) {
            System.out.println("Handle an expected exception");
        }
    }

}
