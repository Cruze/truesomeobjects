package com.example.demo.service;

class DemoException extends RuntimeException {
    public DemoException(String message) {
        super(message);
    }
}
