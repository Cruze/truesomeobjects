package com.example.demo.stringchecker;

import java.util.*;

public class StringChecker {

    private final static Map<Character, Character> symbols = Map.of('}', '{', ']', '[', ')', '(');
    private final static Set<Character> open = Set.of('{', '[', '(');
    private final static Set<Character> close = Set.of('}', ']', ')');

    public static boolean check(String input) {
        Stack<Character> stack = new Stack<>();
        for (Character ch : input.toCharArray()) {
            if (open.contains(ch)) {
                stack.push(ch);
            } else {
                if (close.contains(ch)) {
                    if (stack.isEmpty()) {
                        return false;
                    }
                    Character last = stack.pop();
                    Character match = symbols.get(ch);
                    if (!last.equals(match)) {
                        return false;
                    }
                }
            }
        }

        return stack.isEmpty();
    }

}
