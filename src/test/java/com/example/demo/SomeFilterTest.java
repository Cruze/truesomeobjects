package com.example.demo;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.example.demo.SomeFilter.*;
import static org.junit.jupiter.api.Assertions.*;

class SomeFilterTest {

    @Test
    void empty() {
        //given
        ArrayList<SomeObject> inputs = new ArrayList<>();
        //when
        List<SomeObject> result = filter(inputs);
        //then
        assertTrue(result.isEmpty());
    }

    @Test
    void oneElementWithNulls() {
        //given
        List<SomeObject> inputs = new ArrayList<>();
        inputs.add(new SomeObject(null, null));

        //when
        List<SomeObject> actual = filter(inputs);

        //then
        assertEquals(1, actual.size());
        assertNull(actual.get(0).getFirst());
        assertNull(actual.get(0).getSecond());
    }

    @Test
    void allElementWithNulls() {
        //given
        List<SomeObject> inputs = new ArrayList<>();
        inputs.add(new SomeObject(null, null));
        inputs.add(new SomeObject(null, null));
        inputs.add(new SomeObject(null, null));

        //when
        List<SomeObject> actual = filter(inputs);

        //then
        assertEquals(1, actual.size());
        assertNull(actual.get(0).getFirst());
        assertNull(actual.get(0).getSecond());
    }

    @Test
    void returnTwoOfTwoNotEqualsObjects() {
        //given
        List<SomeObject> inputs = new ArrayList<>();
        inputs.add(new SomeObject(200, 200));
        inputs.add(new SomeObject(100, 100));

        //when
        List<SomeObject> actual = filter(inputs);

        //then
        assertEquals(2, actual.size());
        assertTrue(actual.stream().anyMatch(a -> a.getFirst() == 200 && a.getSecond() == 200));
        assertTrue(actual.stream().anyMatch(a -> a.getFirst() == 100 && a.getSecond() == 100));
    }

    @Test
    void returnOneOfTwoEqualsObject() {
        //given
        List<SomeObject> inputs = new ArrayList<>();
        inputs.add(new SomeObject(100, 200));
        inputs.add(new SomeObject(100, 200));

        //when
        List<SomeObject> actual = filter(inputs);

        //then
        assertEquals(1, actual.size());
        assertTrue(actual.stream().anyMatch(a -> a.getFirst() == 100 && a.getSecond() == 200));
    }

    @Test
    void returnFullestObjectWhenRightIsNull() {
        //given
        List<SomeObject> inputs = new ArrayList<>();
        inputs.add(new SomeObject(100, 200));
        inputs.add(new SomeObject(100, null));

        //when
        List<SomeObject> actual = filter(inputs);

        //then
        assertEquals(1, actual.size());
        assertTrue(actual.stream().anyMatch(a -> a.getFirst() == 100 && a.getSecond() == 200));
    }

    @Test
    void returnFullestObjectWhenLeftIsNull() {
        //given
        List<SomeObject> inputs = new ArrayList<>();
        inputs.add(new SomeObject(100, 200));
        inputs.add(new SomeObject(null, 200));

        //when
        List<SomeObject> actual = filter(inputs);

        //then
        assertEquals(1, actual.size());
        assertTrue(actual.stream().anyMatch(a -> a.getFirst() == 100 && a.getSecond() == 200));
    }

    @Test
    void justCase() {
        //given
        List<SomeObject> inputs = new ArrayList<>();
        inputs.add(new SomeObject(100, 100));
        inputs.add(new SomeObject(200, null));
        inputs.add(new SomeObject(null, null));
        inputs.add(new SomeObject(null, 400));
        inputs.add(new SomeObject(100, 200));

        //when
        List<SomeObject> actual = filter(inputs);

        //then
        assertEquals(4, actual.size());
        assertTrue(actual.stream().anyMatch(a -> a.getFirst() == 100 && a.getSecond() == 100));
        assertTrue(actual.stream().anyMatch(a -> a.getFirst() == 200 && a.getSecond() == null));
        assertTrue(actual.stream().anyMatch(a -> a.getFirst() == 100 && a.getSecond() == 200));
        assertTrue(actual.stream().anyMatch(a -> a.getFirst() == null && a.getSecond() == 400));
    }

}