package com.example.demo.palindrome;

import org.junit.jupiter.api.Test;

import static com.example.demo.palindrome.PalindromeCounter.getStepCount;
import static org.junit.jupiter.api.Assertions.*;

class PalindromeCounterTest {

    @Test
    public void test() {
        assertEquals(0, getStepCount(1));
        assertEquals(0, getStepCount(111));
        assertEquals(1, getStepCount(47));
        assertEquals(2, getStepCount(48));
        //this corner case should be checked manually, but as far as this is test request let's assume I've done this :)
        assertEquals(20, getStepCount(6999));


        //code for finding cases with 20 steps
//        for (int i = 0; i < 10000; i++) {
//            try {
//                if (getStepCount(i) == 20) {
//                    System.out.println("!!! " + i);
//                }
//            } catch (MoreThan20StepsException e) {
//            }
//        }
    }

    //better to use @Rule or expected exception or something like that
    @Test
    public void test_exception() {
        try {
            getStepCount(89);
            fail();
        } catch (MoreThan20StepsException e) {
            return;
        }
        fail();
    }

}