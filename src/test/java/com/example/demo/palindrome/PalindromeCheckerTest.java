package com.example.demo.palindrome;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.example.demo.palindrome.PalindromeChecker.*;
import static org.junit.jupiter.api.Assertions.*;

class PalindromeCheckerTest {

    @Test
    public void test() {
        assertTrue(isPalindrome(BigDecimal.valueOf(121)));
        assertTrue(isPalindrome(BigDecimal.valueOf(1111111)));
        assertTrue(isPalindrome(BigDecimal.valueOf(12321)));
        assertTrue(isPalindrome(BigDecimal.valueOf(123212321)));
        assertFalse(isPalindrome(BigDecimal.valueOf(12)));
        assertFalse(isPalindrome(BigDecimal.valueOf(122)));
        assertFalse(isPalindrome(BigDecimal.valueOf(12323)));
    }

}