package com.example.demo.palindrome;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static com.example.demo.palindrome.NumberTurner.*;
import static java.math.BigDecimal.*;
import static org.junit.jupiter.api.Assertions.*;

class NumberTurnerTest {
    @Test
    public void test() {
        assertEquals(valueOf(111), turnNumber(valueOf(111)));
        assertEquals(valueOf(121), turnNumber(valueOf(121)));
        assertEquals(valueOf(47), turnNumber(valueOf(74)));
        assertEquals(valueOf(123), turnNumber(valueOf(321)));
        assertEquals(new BigDecimal("12345678912345678"), turnNumber(new BigDecimal("87654321987654321")));
    }

}