package com.example.demo.stringchecker;

import org.junit.jupiter.api.Test;

import static com.example.demo.stringchecker.StringChecker.*;
import static org.junit.jupiter.api.Assertions.*;

class StringCheckerTest {

    @Test
    public void test() {
        assertTrue(check("()"));
        assertTrue(check("()[]{}"));
        assertTrue(check("{[]}"));
        assertFalse(check("([)]"));
        assertFalse(check("["));
        assertFalse(check("[[[["));
        assertFalse(check("]"));
        assertTrue(check("[[[]]]"));
        assertTrue(check("[({})]"));
        assertTrue(check(""));
    }

}